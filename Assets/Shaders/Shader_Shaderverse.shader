Shader "Learning/Shader_Shaderverse"
{
    Properties
    {
        _DotTex ("Dot texture", 2D) = "white" {}
        _StripeTex ("Stripe texture", 2D) = "white" {}
        _Color0 ("Color 0", Color) = (1,1,1,1)
        _Stop0 ("Stop 0", float) = 0.3
        _Color1 ("Color 1", Color) = (1,1,1,1)
        _Stop1 ("Stop 1", float) = 0.6
        _Color2 ("Color 2", Color) = (1,1,1,1)
        _Stop2 ("Stop 2", float) = 0.9
        _Steps("Steps", Int) = 1
        _Threshold1("Threshold 1", Float) = 0.1
        _Threshold2("Threshold 2", Float) = 0.1
        _Mask1Blend("Mask 1 strength", Float) = 0.1
        _Mask1Add("Mask 1 addition", Float) = 0.1
        _Mask2Blend("Mask 2 strength", Float) = 0.1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "Utils.cginc"

            sampler2D _DotTex;
            float4 _DotTex_ST;
            float4 _DotTex_TexelSize;
            sampler2D _StripeTex;
            float4 _StripeTex_ST;
            float4 _StripeTex_TexelSize;
            float4 _Color0;
            float _Stop0;
            float4 _Color1;
            float _Stop1;
            float4 _Color2;
            float _Stop2;
            float _Steps;
            float _Threshold1;
            float _Threshold2;
            float _Mask1Blend;
            float _Mask1Add;
            float _Mask2Blend;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float3 normal : NORMAL;
                float3 view : TEXCOORD2;
                float3 worldPos : TEXCOORD3;
                float3 dist : TEXCOORD4;
                float4 vertex : SV_POSITION;
                UNITY_FOG_COORDS(1)
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.normal = UnityObjectToWorldNormal(v.normal);
                o.uv0 = TRANSFORM_TEX(v.uv0, _DotTex);
                o.uv1 = v.uv0 * _StripeTex_ST.xy + _StripeTex_ST.zw;
                o.view = UnityObjectToViewPos(v.vertex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float3 normal = normalize(i.normal);
                float lightPower = dot(normal, _WorldSpaceLightPos0.xyz) * _LightColor0;
                lightPower = max(lightPower, 0.0);
                float2 screenUV = ScreenUV(i.view);
                float t1 = _Threshold1;
                float t2 = _Threshold2;

                fixed4 steppedMask = Posterize(lightPower, _Steps);
                
                fixed4 dot = tex2D(_DotTex, screenUV * i.uv0.xy);
                fixed4 stripe = tex2D(_StripeTex, screenUV * i.uv1.xy);

                fixed4 stripeMask = smoothstep(_Stop1, _Stop1 + t1, lightPower) + smoothstep(_Stop1, _Stop1 - t1, lightPower);
                fixed4 mask1 = lerp(0, stripe, (1 - stripeMask) * _Mask1Blend) * smoothstep(_Stop1, _Stop1 + t1, lightPower);
                mask1 += smoothstep(_Stop1, _Stop1 + t1, lightPower);
                
                fixed4 dotMask = smoothstep(_Stop2, _Stop2 + t2, lightPower) + smoothstep(_Stop2, _Stop2 - t2, lightPower);
                fixed4 mask2 = lerp(0, dot, (1 - dotMask) * _Mask2Blend) * smoothstep(_Stop2, _Stop2 - t2, lightPower);
                mask2 += smoothstep(_Stop2, _Stop2 + t2, lightPower);
                
                fixed4 totalMask = mask1 * (mask2 + _Stop1);
                
                fixed4 output = _Color0;
                output = lerp(output, _Color1, smoothstep(_Stop0, _Stop1, totalMask));
                output = lerp(output, _Color2, smoothstep(_Stop1, _Stop2, totalMask));
                return output;
            }
            ENDCG
        }
    }
}
