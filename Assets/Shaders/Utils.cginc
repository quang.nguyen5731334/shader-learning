﻿inline float2 ScreenUV(float3 view)
{
    float3 origin = unity_ObjectToWorld._m03_m13_m23;
    float depth = -mul(UNITY_MATRIX_V, float4(origin, 1.0)).z;
    float2 uv = view.xy / view.z;
    uv -= 0.5;
    uv.xy *= _ScreenParams.x / _ScreenParams.y;
    uv *= depth / UNITY_MATRIX_P._m11;
    return uv;
};

inline float4 Overlay(float4 base, float4 blend, float opacity)
{
    float4 r1 = 1.0 - 2.0 * (1.0 - base) * (1.0 - blend);
    float4 r2 = 2.0 * base * blend;
    float4 limit = step(base, 0.5);
    float4 output = r2 * limit + (1 - limit) * r1;
    return lerp(base, output, opacity);
}

inline float4 Remap(float4 val, float2 inRange, float2 outRange) {
	return outRange.x + (val - inRange.x) * (outRange.y - outRange.x) / (inRange.y - inRange.x);
}

inline float4 Posterize(float4 val, float steps) {
	return floor(val / (1.0 / steps)) * (1.0 / steps);
}

inline float3 PosterizeNormal(float3 val, float steps) {
    return floor(val / (1.0 / steps)) * (1.0 / steps);
}

inline float4 InverseLerp(float from, float to, float val) {
	return (val - from) / (to - from);
}

inline float4 Outline(float4 vertPos, float width)
{
    float4x4 scaleMatrix;
    scaleMatrix[0][0] = 1.0 + width;
    scaleMatrix[0][1] = 0.0;
    scaleMatrix[0][2] = 0.0;
    scaleMatrix[0][3] = 0.0;
    scaleMatrix[1][0] = 0.0;
    scaleMatrix[1][1] = 1.0 + width;
    scaleMatrix[1][2] = 0.0;
    scaleMatrix[1][3] = 0.0;
    scaleMatrix[2][0] = 0.0;
    scaleMatrix[2][1] = 0.0;
    scaleMatrix[2][2] = 1.0 + width;
    scaleMatrix[2][3] = 0.0;
    scaleMatrix[3][0] = 0.0;
    scaleMatrix[3][1] = 0.0;
    scaleMatrix[3][2] = 0.0;
    scaleMatrix[3][3] = 1.0 + width;

    return mul(scaleMatrix, vertPos);
}