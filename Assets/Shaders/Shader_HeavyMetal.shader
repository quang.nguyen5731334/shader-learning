Shader "Learning/Shader_HeavyMetal"
{
    Properties
    {
        _LightColor("Light color", Color) = (1,1,1,1)
        _ShadowColor("Shadow color", Color) = (0,0,0,1)
        _Smoothness("Smoothness", Float) = 1
        _SpecularStrength("Specular strength", Float) = 1
        _HighlightMin("Highlight min threshold", Float) = 1
        _HighlightStrength("Highlight strength", Float) = 1
        _Steps("Steps", Int) = 1
        
        _FresnelSteps("Fresnel steps", Int) = 1
        _FresnelScale("Fresnel scale", Float) = 1
        _FresnelPower("Fresnel power", Float) = 1
        _FresnelBlend("Fresnel blend", Float) = 1
        _FresnelColor("Fresnel color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fwdbase

            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "Utils.cginc"

            float4 _LightColor;
            float4 _ShadowColor;
            float _Smoothness;
            float _SpecularStrength;
            float _HighlightMin;
            float _HighlightStrength;
            float _FresnelScale;
            float _FresnelPower;
            float _FresnelBlend;
            float4 _FresnelColor;
            half _FresnelSteps;
            half _Steps;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 world : TEXCOORD1;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float3 world : TEXCOORD1;
                float3 ambient : TEXCOORD2;
                float fresnel : TEXCOORD3; 
                float3 normal: NORMAL;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.normal = UnityObjectToWorldNormal(v.normal);
                o.world = WorldSpaceViewDir(v.vertex);
                o.ambient = Shade4PointLights(unity_4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0,
                unity_LightColor[0].rgb, unity_LightColor[1].rgb, unity_LightColor[2].rgb, unity_LightColor[3].rgb,
                unity_4LightAtten0, v.world, v.normal);
                o.fresnel = _FresnelBlend + _FresnelScale * pow(1 + dot(normalize(ObjSpaceViewDir(v.vertex)), v.normal), _FresnelPower);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float3 normal = normalize(i.normal);
                float lightPower = dot(normal, _WorldSpaceLightPos0.xyz) * _LightColor0;
                lightPower = max(lightPower, 0.0);

                float3 viewDir = normalize(_WorldSpaceCameraPos.xyz - i.world.xyz);
                float3 specularDir = reflect(_WorldSpaceLightPos0.xyz, normalize(i.normal));
                float specularDensity = max(dot(specularDir, viewDir), 0.0);
                specularDensity = pow(specularDensity, _Smoothness);
                
                float3 reflectDir = reflect(-viewDir, i.normal);
                float4 reflection = UNITY_SAMPLE_TEXCUBE_LOD(unity_SpecCube0, reflectDir, 3);
                float4 lighting = float4(i.ambient.x, i.ambient.y, i.ambient.z, 1);
                float4 steppedMask = Posterize(lightPower, _Steps);
                float4 metallic = lerp(_ShadowColor, _LightColor, steppedMask);
                float4 fresnel = Posterize(1.0 - i.fresnel, _FresnelSteps);
                float4 output = lerp(metallic, _FresnelColor, saturate(fresnel));
                output += specularDensity * _SpecularStrength;
                output += smoothstep(_HighlightMin, 1, lightPower * 0.98) * _HighlightStrength;
                return output;
            }
            ENDCG
        }
    }
}
