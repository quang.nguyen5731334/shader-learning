Shader "Learning/Shader_Rocky"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _NormalMap("Normal map", 2D) = "" {}
        _NormalStrength("Normal strength", Float) = 1
        _NormalSteps("Normal steps", Int) = 1
        
        _CreviceColor("Crevice color", Color) = (0,0,0,1)
        _LightColor("Light color", Color) = (1,1,1,1)
        _ShadowColor("Shadow color", Color) = (0,0,0,1)
        
        _OutlineColor("Outline color", Color) = (1,1,1,1)
        _OutlineWidth("Outline width", float) = 0.2
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "Utils.cginc"
            
            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _NormalMap;
            float4 _NormalMap_ST;
            half _NormalSteps;
            float _NormalStrength;
            float4 _CreviceColor;
            float4 _LightColor;
            float4 _ShadowColor;
            float4 _OutlineColor;
            float _OutlineWidth;

            struct appdata
            {
                float4 vertex : POSITION;
                float4 tangent : TANGENT;
                float4 normal : NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float3 tangent : TEXCOORD1;
                float3 normal : TEXCOORD2;
                float3 binormal : TEXCOORD3;
                float4 vertex : SV_POSITION;
            };


            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.normal = UnityObjectToWorldNormal(v.normal);
                o.tangent = UnityObjectToWorldNormal(v.tangent);
                o.binormal = normalize(cross(o.normal, o.tangent));
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                i.tangent = normalize(i.tangent);
                i.binormal = normalize(i.binormal);
                i.normal = normalize(i.normal);
                float3x3 tbn = float3x3(i.tangent, i.binormal, i.normal);
                float3 normal = UnpackNormal(tex2D(_NormalMap, i.uv));
                normal *= float3(normal.xy * _NormalStrength, lerp(1, normal.z, saturate(_NormalStrength)));
                normal = PosterizeNormal(normal, _NormalSteps);
                normal = normalize(mul(normal, tbn));

                float diffuse = dot(i.normal, _WorldSpaceLightPos0.xyz);
                diffuse = max(diffuse, 0);
                
                float lightPower = dot(normal, _WorldSpaceLightPos0.xyz);
                lightPower = max(lightPower, 0.0) * _LightColor0;
                
                fixed4 creviceMask = tex2D(_MainTex, i.uv);

                fixed4 steppedMask = Remap(clamp(0, 0.5, Posterize(diffuse, 4)), float2(0, 0.5), float2(0, 1));
                fixed4 output = lerp(_ShadowColor, _LightColor, lightPower * steppedMask);
                output = lerp(_CreviceColor, output, creviceMask);
                
                return output;
            }
            ENDCG
        }
        
        Pass
        {
            Cull Front
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Utils.cginc"
            
            uniform half4 _OutlineColor;
            uniform float _OutlineWidth;
            
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(Outline(v.vertex, _OutlineWidth));
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return _OutlineColor;
            }
            ENDCG
        }
    }
}
