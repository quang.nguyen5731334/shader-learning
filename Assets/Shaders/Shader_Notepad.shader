Shader "Learning/Shader_Notepad"
{
    Properties
    {
        _OutlineColor("Outline color", Color) = (1,1,1,1)
        _HatchColor("Hatch color", Color) = (1,1,1,1)
        _LineColor("Paper line color", Color) = (1,1,1,1)
        _OutlineWidth("Outline width", float) = 0.2
        _NoiseTex("Noise texture", 2D) = "white" {}
        _NoiseStrength("Noise strength", float) = 1
        _NoiseBlend("Noise blend", float) = 1
        
        _CrosshatchTiling("Crosshatch tiling", Vector) = (1,1,1,1)
        _CrosshatchOffset("Crosshatch offset", Vector) = (0,0,1,1)
        _CrosshatchThickness("Crosshatch thickness", float) = 0.7
        _CrosshatchRotation("Crosshatch rotation", float) = 0
        _HatchThreshold("Hatch threshold", Vector) = (0,1,0,0)
        _TopThreshold("Top threshold", float) = 0.25
        
        _LineFrequency("Line frequency", float) = 1
        _LineThickness("Line thickness", float) = 1
        _LineOpacity("Line opacity", float) = 0.4
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "Utils.cginc"
            
            uniform half4 _CrosshatchTiling;
            half4 _CrosshatchOffset;
            float _CrosshatchThickness;
            float _CrosshatchRotation;
            half2 _HatchThreshold;
            float _TopThreshold;
            half4 _HatchColor;
            half4 _LineColor;
            float _LineFrequency;
            float _LineThickness;
            float _LineOpacity;
            sampler2D _NoiseTex;
            uniform float4 _NoiseTex_ST;
            float _NoiseStrength;
            float _NoiseBlend;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float2 noiseUV : TEXCOORD1;
                float3 normal : NORMAL;
                float4 vertex : SV_POSITION;
                float3 view : TEXCOORD2;
                float4 worldPos : TEXCOORD3;
                UNITY_FOG_COORDS(1)
            };


            v2f vert (appdata v)
            {
                v2f o;
                UNITY_INITIALIZE_OUTPUT(v2f, o);
                o.vertex = UnityObjectToClipPos(v.vertex);
                
                o.normal = UnityObjectToWorldNormal(v.normal);
                o.uv = v.uv;
                o.noiseUV = v.uv.xy * _NoiseTex_ST.xy + _NoiseTex_ST.zw;
                o.view = UnityObjectToViewPos(v.vertex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);

                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float4 noise = tex2D(_NoiseTex, i.noiseUV);
                float angle = _CrosshatchRotation * (3.1415926f/180.0f);
                float2 pivot = float2(0.5, 0.5);
                fixed2 uvs = float3(
                    i.view.x,
                    i.view.y,
                    i.view.z
                    );

                uvs -= pivot;

                float cosAngle = cos(angle);
                float sinAngle = sin(angle);
                float2x2 rot = float2x2(cosAngle, -sinAngle, sinAngle, cosAngle);
                rot *= 0.5;
                rot += 0.5;
                rot = rot * 2 - 1;
                
                uvs = mul(uvs.xy, rot);
                uvs += pivot;
                
                float3 normal = normalize(i.normal);
                float lightPower = dot(normal, _WorldSpaceLightPos0.xyz);
                lightPower = max(lightPower, 0.0);

                float4 hatch = sin(uvs.y * _CrosshatchTiling.y + _CrosshatchOffset.y) <= _CrosshatchThickness;
                float4 crossHatch = (sin(uvs.x * _CrosshatchTiling.x + _CrosshatchOffset.x) <= _CrosshatchThickness) *
                    (sin(uvs.y * _CrosshatchTiling.y + _CrosshatchOffset.y) <= _CrosshatchThickness);

                float4 paperLine = fmod(i.vertex.y * normalize(length(ObjSpaceViewDir(i.vertex))) + 1, 1 - _LineFrequency) > _LineThickness;
                
                UNITY_APPLY_FOG(i.fogCoord, col);
                float4 lightMask = Overlay(noise * _NoiseStrength, (lightPower > _TopThreshold) + lightPower, _NoiseBlend);
                float4 mask = lerp(
                    crossHatch,
                    hatch,
                    (lightMask >= _HatchThreshold.x) * (lightMask <= _HatchThreshold.y) + noise
                    );
                mask = lerp(mask, 1, lightMask);
                mask = mask + noise;
                float4 colLine = lerp(_LineColor, 1, paperLine + (1 - _LineOpacity));
                float4 colHatch = lerp(_HatchColor, 1, mask);
                float4 output = colLine * clamp(0, 1, colHatch);
                return output;
            }
            ENDCG
        }
        
        Pass
        {
            Cull Front
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Utils.cginc"
            
            uniform half4 _OutlineColor;
            uniform float _OutlineWidth;
            
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(Outline(v.vertex, _OutlineWidth));
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return _OutlineColor;
            }
            ENDCG
        }
    }
}
