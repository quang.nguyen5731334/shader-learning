Shader "Unlit/Shader_Ember"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _DistortTex("Distort texture", 2D) = "" {}
        _DistortStrength("Distort strength", Float) = 1
        _DistortX("Distort X", Float) = 1
        _DistortY("Distort Y", Float) = 1
        
        _ColorA("Color A", Color) = (1,1,1,1)
        _ColorB("Color B", Color) = (0,0,0,1)
        _ColorC("Color C", Color) = (1,1,1,1)
        _Offset("Offset", Float) = 1
        _Edge1("Edge 1", Float) = 1
        _Edge2("Edge 2", Float) = 1
        
        _MaskHeight("Mask height", Float) = 1
        _Cutoff("Alpha cutoff", Float) = 1
        
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent" }
        Blend SrcAlpha OneMinusSrcAlpha
//        Cull Front
//        ZWrite Off
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Utils.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float3 normal : TEXCOORD2;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _DistortTex;
            float4 _DistortTex_ST;
            float _DistortStrength;
            float _DistortX;
            float _DistortY;

            float4 _ColorA;
            float4 _ColorB;
            float4 _ColorC;
            float _Offset;
            float _Edge1;
            float _Edge2;
            
            float _MaskHeight;
            float _Cutoff;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.normal = UnityObjectToWorldNormal(v.normal);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.uv1 = TRANSFORM_TEX(v.uv, _DistortTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 distort = tex2D(_DistortTex, i.uv1) * _DistortStrength;
                fixed4 noise = tex2D(_MainTex, fixed2((i.uv.x + _Time.x * _DistortX) + distort.g, (i.uv.y + _Time.x * _DistortY) + distort.r));
                fixed4 yMask = lerp(float4(2,2,2,2), float4(0, 0, 0, 0), (i.uv.y + _MaskHeight));
                fixed4 colA = lerp(_ColorA, _ColorA, i.uv1.y + _Offset);
                fixed4 colB = lerp(_ColorB, _ColorB, i.uv1.y + _Offset);
                fixed4 colC = lerp(_ColorC, _ColorC, i.uv1.y + _Offset);

                noise += yMask;
                fixed4 flame = saturate(noise * _Cutoff);
                fixed4 flameCol = flame * colA;
                
                fixed4 rim1 = saturate((noise + _Edge1) * _Cutoff);
                fixed4 rimCol1 = colB;

                fixed4 rim2 = saturate((noise + _Edge2) * _Cutoff);
                fixed4 rimCol2 = colC;
                
                fixed4 output = lerp(rimCol1, flameCol, flame > 0.9);
                fixed4 o1 = lerp(rimCol2, output, rim1 > 0.9);
                fixed4 o2 = lerp(rimCol2, o1, rim2 > 0.9);
                o2.a = rim2;
                return o2;
            }
            ENDCG
        }
    }
}
