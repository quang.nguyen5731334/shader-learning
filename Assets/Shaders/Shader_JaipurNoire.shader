Shader "Learning/Shader_JaipurNoire"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _LightColor("Light color", Color) = (1,1,1,1)
        _ShadowColor("Shadow color", Color) = (0,0,0,1)
        _NoiseTiling("Noise tiling", Float) = 1
        _Steps("Steps", Int) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Utils.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 world : TEXCOORD1;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float3 world : TEXCOORD1;
                float3 view : TEXCOORD2;
                float4 vertex : SV_POSITION;
                float3 normal: NORMAL;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _LightColor;
            float4 _ShadowColor;
            float _NoiseTiling;
            half _Steps;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.normal = UnityObjectToWorldNormal(v.normal);
                o.world = WorldSpaceViewDir(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.view = UnityObjectToViewPos(v.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 screenUV = i.view * _NoiseTiling;
                
                float3 normal = normalize(i.normal);
                float lightPower = dot(normal, _WorldSpaceLightPos0.xyz);
                lightPower = max(lightPower, 0.0);
                float4 steppedMask = Posterize(lightPower, _Steps);
                float4 x = smoothstep(0.5, 1, steppedMask);
                
                // sample the texture
                fixed4 col = tex2D(_MainTex, screenUV);
                float4 y = step(0.5, x);
                float4 z = step(x, 0.5);
                float4 w = step(x, 0.25);
                float4 output = lerp(_LightColor, x * col, z);
                output = lerp(output, (x + col) * _ShadowColor, w);
                return output;
            }
            ENDCG
        }
    }
}
