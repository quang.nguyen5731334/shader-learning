Shader "Learning/Shader_Hologram"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        
        _InnerNoise ("Inner noise", 2D) = "white" {}
        _InnerNoiseDetail("Inner noise detail", 2D) = "white" {}
        _OutlineNoise("Outline noise", 2D) = "white" {}
        _InnerNoiseTiling("Inner noise tiling", Float) = 1
        _InnerNoiseSpeed("Inner noise speed", Float) = 1
        _InnerNoiseDetailTiling("Inner noise detail tiling", Float) = 1
        _OutlineNoiseTiling("Outline noise tiling", Float) = 1
        _OutlineNoiseSpeed("Outline noise speed", Float) = 1
        _LineFrequency("Line frequency", Float) = 1
        _LineThickness("Line thickness", Float) = 1
        _LineMaskBlend("Line mask blend", Float) = 1
        _LineMaskSpeed("Line mask speed", Float) = 1
        _LineBlend("Line blend", Float) = 1
        
        _UVMaskBlend("UV mask blend", Float) = 1
        
        _FresnelScale("Fresnel scale", Float) = 1
        _FresnelPower("Fresnel power", Float) = 1
        _FresnelBlend("Fresnel blend", Float) = 1
        _FresnelThickness("Fresnel thickness", Float) = 1
        
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent"}
        Blend SrcAlpha OneMinusSrcAlpha
//        ZWrite Off
        Cull Back
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Utils.cginc"

            float4 _Color;
            
            sampler2D _InnerNoise;
            float4 _InnerNoise_ST;
            sampler2D _InnerNoiseDetail;
            float4 _InnerNoiseDetail_ST;
            sampler2D _OutlineNoise;
            float4 _OutlineNoise_ST;
            float _InnerNoiseTiling;
            float _InnerNoiseDetailTiling;
            float _InnerNoiseSpeed;
            float _OutlineNoiseTiling;
            float _OutlineNoiseSpeed;
            
            float _LineFrequency;
            float _LineThickness;
            float _LineBlend;
            float _LineMaskBlend;
            float _LineMaskSpeed;
            
            float _UVMaskBlend;
            float _FresnelScale;
            float _FresnelPower;
            float _FresnelBlend;
            float _FresnelThickness;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float3 normal: NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float3 view : TEXCOORD3;
                float fresnel : TEXCOORD4;
                float3 normal: NORMAL;
                float4 vertex : SV_POSITION;
            };
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.normal = UnityObjectToWorldNormal(v.normal);
                o.uv = TRANSFORM_TEX(v.uv, _InnerNoise);
                o.uv1 = TRANSFORM_TEX(v.uv1, _InnerNoiseDetail);
                o.uv2 = TRANSFORM_TEX(v.uv2, _OutlineNoise);
                o.fresnel = _FresnelBlend + _FresnelScale * pow(1 + dot(normalize(ObjSpaceViewDir(v.vertex)), v.normal), _FresnelPower);
                o.view = UnityObjectToViewPos(v.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 uv = ScreenUV(i.view);
                fixed4 innerNoise = tex2D(_InnerNoise, uv * _InnerNoiseTiling + (0, _Time.y * _InnerNoiseSpeed));
                fixed4 innerNoiseDetail = tex2D(_InnerNoiseDetail, uv * _InnerNoiseDetailTiling);
                fixed4 outerNoise = tex2D(_OutlineNoise, uv * _OutlineNoiseTiling - (0, _Time.y * _OutlineNoiseSpeed));
                fixed4 fresnel = (1,1,1,1);
                fixed4 uvBlend = smoothstep( _UVMaskBlend - 0.1, _UVMaskBlend, (1.0 - i.uv.y));
                fixed4 edge = step(1 - _FresnelThickness, saturate(1.0 - i.fresnel));
                fixed4 alpha = edge * uvBlend;
                alpha.a *= outerNoise;
                fresnel.a = alpha.a;

                fixed4 lines = (1,1,1,1);
                fixed4 lineMask = fmod(uv.y + (_Time.y * _LineMaskSpeed), 1.0 - _LineFrequency) < _LineThickness;
                fixed4 lineAlpha = lineMask * (1.0 - smoothstep(0.5, 0.9, uv.y * _LineMaskBlend));
                lineAlpha.a *= innerNoiseDetail;
                lineAlpha.a *= innerNoise;
                lines.a = saturate(lineAlpha.a * _LineBlend);

                fixed4 output = _Color * saturate(fresnel + lines);
                return output;
            }
            ENDCG
        }
    }
}
