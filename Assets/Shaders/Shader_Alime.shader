Shader "Learning/Shader_Alime"
{
    Properties
    {
        _BaseColor("Base color", Color) = (1,1,1,1)
        _ShadowColor("Shadow color", Color) = (0,0,0,1)
        _ShadowThreshold("Shadow threshold", float) = 0.5
        
        _OutlineColor("Outline color", Color) = (1,1,1,1)
        _OutlineWidth("Outline width", float) = 0.2
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "Utils.cginc"

            float4 _BaseColor;
            float4 _ShadowColor;
            half _ShadowThreshold;

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.normal = UnityObjectToWorldNormal(v.normal);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float3 normal = normalize(i.normal);
                float lightPower = dot(normal, _WorldSpaceLightPos0.xyz) * _LightColor0;
                lightPower = max(lightPower, 0.0);
                
                return lerp(_BaseColor, _ShadowColor, lightPower < _ShadowThreshold);
            }
            ENDCG
        }
        
        Pass
        {
            Cull Front
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Utils.cginc"
            
            uniform half4 _OutlineColor;
            uniform float _OutlineWidth;
            
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(Outline(v.vertex, _OutlineWidth));
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return _OutlineColor;
            }
            ENDCG
        }
    }
}
