Shader "Learning/Shader_Aqua"
{
    Properties
    {
        _CausticTex("Caustic texture", 2D) = "white" {}
        _MainColor("Main color", Color) = (1,1,1,1)
        _SecondaryColor("Secondary color", Color) = (0,0,0,1)
        
        _LightThreshold("Light threshold", Float) = 1
        _FresnelScale("Fresnel scale", Float) = 1
        _FresnelPower("Fresnel power", Float) = 1
        _FresnelBlend("Fresnel blend", Float) = 1
        _FresnelThickness("Fresnel thickness", Float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Utils.cginc"

            sampler2D _CausticTex;
            fixed4 _CausticTex_ST;
            fixed4 _MainColor;
            fixed4 _SecondaryColor;
            
            float _LightThreshold;
            float _FresnelScale;
            float _FresnelPower;
            float _FresnelBlend;
            float _FresnelThickness;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal: NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 view : TEXCOORD1;
                float fresnel : TEXCOORD2;
                float3 world : TEXCOORD3;
                float3 normal: NORMAL;
            };
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.normal = UnityObjectToWorldNormal(v.normal);
                o.uv = TRANSFORM_TEX(v.uv, _CausticTex);
                o.fresnel = _FresnelBlend - _FresnelScale * pow(1 + dot(normalize(ObjSpaceViewDir(v.vertex)), v.normal), _FresnelPower);
                o.view = UnityObjectToViewPos(v.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 caustic = tex2D(_CausticTex, i.uv);
                
                float3 normal = normalize(i.normal);
                float lightPower = dot(normal, _WorldSpaceLightPos0.xyz);
                lightPower = max(lightPower, 0.0);

                float3 viewDir = normalize(_WorldSpaceCameraPos.xyz - i.world.xyz);
                float3 specularDir = reflect(_WorldSpaceLightPos0.xyz, normalize(i.normal));
                float specularDensity = max(dot(specularDir, viewDir), 0.0);
                specularDensity = pow(specularDensity, 0.8);
                
                fixed4 edge = step(1 - _FresnelThickness, saturate(i.fresnel));
                fixed4 highlight = edge + step(_LightThreshold, lightPower);
                fixed4 aquaMask = step(0.4, i.uv.y);
                fixed4 mask = step(0.4, specularDensity) * (1.0 - aquaMask);
                fixed4 aquaColor = lerp(_MainColor, _SecondaryColor, caustic);

                fixed4 output = aquaColor + highlight;
                return output;
            }
            ENDCG
        }
    }
}
